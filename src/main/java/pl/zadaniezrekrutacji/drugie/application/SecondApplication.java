package pl.zadaniezrekrutacji.drugie.application;

public class SecondApplication {

	public static boolean isLeap(int year) {
		if (year % 4 == 0) {
			return true;					// Sprawdzanie czy rok jest przestÍpny
		} else
			return false;
	}

	public static boolean verifyDate(int day, int month, int year) {
		
		if(day < 1 || day > 31 || month < 1 || month > 12 || year < 2001 || year > 2099 ) return false; // Zerowe / ujemne daty i zakres daty
		
		if(month == 2 && isLeap(year)) {
			if(day > 29) {
				return false;
			} else if(month == 2 && (day > 28)){ 			// Luty
				return false;
			}
		}
	
		if(month % 2 == 1 && day == 31 ) return false;
		
		return true;
	}

	public static void main(String[] args) {
	//							  dzien / miesiac / rok
		System.out.println(verifyDate(1, 02, 2010));
		System.out.println(verifyDate(29, 2, 2012));
		System.out.println(verifyDate(29, 02, 2011));
		System.out.println(verifyDate(14, 04, 966));
		System.out.println(verifyDate(50, 02, 2010));
		System.out.println(verifyDate(31, 7, 2010));
		System.out.println(verifyDate(3, 3, 2003));
		System.out.println(verifyDate(03, 03, 2003));
		
	}
}
